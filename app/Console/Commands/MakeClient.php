<?php

namespace App\Console\Commands;

use App\Models\OAuth\Client;
use App\Models\OAuth\Scope;
use Illuminate\Console\Command;

class MakeClient extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:client {name} {id} {grant}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates an OAuth client.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('grant') == 'manage') {
            $scopes = Scope::where('id', 'LIKE', '%manage%')->get();
        } elseif ($this->argument('grant') == 'write') {
            $scopes = Scope::where('id', 'LIKE', '%write%')->get();
        } else {
            $scopes = Scope::where('id', 'LIKE', '%read%')->get();
        }

        $client = Client::create([
            'id' => $this->argument('id'),
            'name' => $this->argument('name'),
            'secret' => hash_hmac('sha1', 'O rato roeu a roupa do rei de rome.', env('APP_KEY'))
        ]);

        $client->scopes()->sync($scopes);

        $this->info('Client Secret: ' . $client->secret);
    }
}
