<?php

namespace App\Services\PaymentGateways;

use jlcd\Cielo\Resources\CieloPayment;
use jlcd\Cielo\Resources\CieloCustomer;
use jlcd\Cielo\Resources\CieloOrder;
use jlcd\Cielo\Resources\CieloCreditCard;
use jlcd\Cielo\Responses\ResourceResponse;
use jlcd\Cielo\Requests\TokenizeCardRequest;
use jlcd\Cielo\Exceptions\ResourceErrorException;


use Cielo\API30\Merchant;
use Cielo\API30\Ecommerce\Environment;
use Cielo\API30\Ecommerce\Sale;
use Cielo\API30\Ecommerce\CieloEcommerce;
use Cielo\API30\Ecommerce\Payment;

use Cielo\API30\Ecommerce\Request\CieloRequestException;


class CieloGateway implements GatewayInterface
{
    
    private $merchant    = null;
    private $environment = null;
    
    
    /**
     * Create a new gateway instance.
     *
     * @return void
     */
    public function __construct()
    {
        $config = config('payment.gateways.cielo');
        $this->merchant    = new Merchant($config['merchant_id'], $config['merchant_secret']);
        $this->environment = $config['environment'] == 'production' ? Environment::production() : Environment::sandbox();
    }

    /**
     * Retrieves access token.
     *
     * @return string $access_token
     */
    public function authorize()
    {
        // TODO
    }

    /**
     * Purchase items from a invoice.
     *
     * @param  string  $code
     * @return \PagSeguro\Parsers\Transaction\Response
     */
    public function purchase($invoice)
    {
        dd($invoice);
    }
    
    public function payment(CieloPayment $cieloPayment, CieloOrder $cieloOrder, CieloCustomer $cieloCustomer = null)
    {
        $sale = new Sale($cieloOrder->getId());

        $payment = $sale->payment($cieloPayment->getValue(), $cieloPayment->getInstallments());

        $creditCard = $cieloPayment->getCreditCard();

        $payment->setType(Payment::PAYMENTTYPE_CREDITCARD);
        $paymentCard = $payment->creditCard($creditCard->getSecurityCode(), $creditCard->getBrand());

        if ($creditCard->getToken()) {
            $paymentCard->setCardToken($creditCard->getToken());
        } else {
            $paymentCard->setExpirationDate($creditCard->getExpirationDate())
                        ->setCardNumber($creditCard->getCardNumber())
                        ->setHolder($creditCard->getHolder());
        }

        if ($cieloCustomer) {
            $customer = $sale->customer($cieloCustomer->getName());
        }

        try {
            $sale = (new CieloEcommerce($this->merchant, $this->environment))->createSale($sale);

            $paymentData = $sale->getPayment()->jsonSerialize();
            $paymentStatus = $sale->getPayment()->getStatus();
            $paymentMessage = $sale->getPayment()->getReturnMessage();
        } catch (CieloRequestException $e) {
            $error = $e->getCieloError();
            throw new ResourceErrorException($error->getMessage(), $error->getCode());
        }

        return new ResourceResponse($paymentStatus, $paymentMessage, $paymentData);
    }

    /**
     * Get transaction data by code.
     *
     * @param  string  $code
     * @return \PagSeguro\Parsers\Transaction\Response
     */
    public function getTransaction($code)
    {
        // TODO
    }

    /**
     * Get converted transaction status.
     *
     * @param  string  $code
     * @return string  pending | accepted | rejected | null
     */
    public function getTransactionStatus($code)
    {
        // TODO
    }
}
