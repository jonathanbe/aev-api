<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'excerpt',
        'content',
        'thumbnail_id'
    ];
    
    static $validations = [
	    'post.title' => 'required',
	    'post.slug' => 'required',
	    'post.excerpt' => 'required',
	    'post.content' => 'required'
    ];

    /**
     * Get the category record associated with the post.
     */
    
    public function gallery()
    {
        return $this->belongsToMany('App\Models\Media', 'post_media')->withPivot('caption', 'order');
    }
    
    public function categories()
    {
        return $this->belongsToMany('App\Models\Category', 'post_category');
    }
    
    public function thumbnail()
    {
        return $this->belongsTo('App\Models\Media');
    }
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'content' => 'array',
    ];
    
    /**
     * Search Method
     * @param type $q
     * @return type
     */
    public function scopeSearch($query, $q)
    {
        return $query->where('title', "LIKE", "%$q%")
            ->orWhere("slug", "LIKE", "%$q%")
            ->orWhere('excerpt', 'LIKE', "%$q%");
    }
}
