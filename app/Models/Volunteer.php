<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Volunteer extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'civil_status', 'birthday', 'ocupation',
        'religion', 'reason', 'suggestions', 'abilities', 'days',
        'shifts', 'person_id'
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id', 'civil_status', 'birthday', 'ocupation',
        'religion', 'reason', 'suggestions', 'abilities', 'days',
        'shifts', 'person_id', 'person', 'created_at', 'updated_at',
        'deleted_at'
    ];
    
    static $validations = [
	    'volunteer.civil_status' => 'required',
	    'volunteer.birthday' => 'required|date_format:Y-m-d',
	    'volunteer.ocupation' => 'required',
	    'volunteer.abilities' => 'required',
	    'volunteer.days' => 'required',
	    'volunteer.shifts' => 'required',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'days' => 'array',
        'shifts' => 'array',
    ];

    /**
     * Get the person record associated with the volunteer.
     */
    public function person()
    {
        return $this->belongsTo('App\Models\Person');
    }
    
    /**
     * Search Method
     * @param type $q
     * @return type
     */
    public function scopeSearch($query, $q)
    {
        return $query->whereHas('person', function($query) use($q) {
            $query->whereRaw('first_name || " " || last_name LIKE ?', ["%$q%"]);
            $query->orWhere('email', 'LIKE', "%$q%");
            $query->orWhere('cpf', 'LIKE', "%$q%");
        });
    }
}
