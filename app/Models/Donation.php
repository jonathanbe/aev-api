<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use App\Events\DonationCreated;
use App\Models\IPurchasable;
use App\Notifications\DonationInvoice;
use Carbon\Carbon;

class Donation extends Model implements IPurchasable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value',
        'institution_id',
        'invoice_id'
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'value',
        'institution',
        'institution_id',
        'invoice',
        'invoice_id',
        'created_at',
        'deleted_at'
    ];
    
    static $validations = [
	    'donation.value' => 'required',
	    'donation.institution_id' => 'required',
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $events = [
        // 'created' => DonationCreated::class,
    ];

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice');
    }

    public function institution()
    {
        return $this->belongsTo('App\Models\Institution');
    }

    /**
     * Search Method
     * @param type $q
     * @return type
     */
    public function scopeSearch($query, $q)
    {
        if(isset($q['init'])){
            $q['init'] .= " 00:00:00";
            $query->where('created_at','>=', $q['init']);
        }
        if (isset($q['end'])){
            $q['end'] .= " 23:59:59";
            $query->where('created_at','<=', $q['end']);
        }
        return $query;
    }

    public function getDescription()
    {
        return 'Doação para instituição de João Pessoa';
    }

    public function getPrice()
    {
        return $this->attributes['value'];
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendDonatorNotification()
    {
        $this->notify(new DonationInvoice($this));
    }

    /**
     * Route notifications for the mail channel.
     *
     * @return string
     */
    public function routeNotificationForMail()
    {
        return $this->invoice->person->email;
    }}
