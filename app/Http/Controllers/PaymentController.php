<?php

namespace App\Http\Controllers;

use App\Models\Donation;
use App\Services\PaymentService;
use App\Models\Person;
use App\Models\Invoice;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    protected $service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PaymentService $service)
    {
        $this->service = $service;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return [\AppModels\OAuth\User]
     */
    public function notification(Request $request)
    {
        return response($this->service->callback($request), 200);
    }
}
