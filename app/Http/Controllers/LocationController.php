<?php

namespace App\Http\Controllers;

use Countries;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class LocationController extends Controller
{
    /**
     * Paginate resource.
     *
     * @return void
     */
    public function index()
    {
        return response(Countries::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return [\AppModels\OAuth\User]
     */
    public function cep(Request $request)
    {
        $input = $request->all();
        $client = new Client();
        $response = $client->get('http://viacep.com.br/ws/' . $input['number'] . '/json');

        return response($response->getBody(), 200);
    }
}
