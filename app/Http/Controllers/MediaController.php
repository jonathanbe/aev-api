<?php 

namespace App\Http\Controllers;
 
//use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Models\Media;

 
//use Request;
 
class MediaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Paginate images.
     *
     * @return void
     */
    public function images (Request $request) {
        $medias = Media::where('disk', '=', 'images')
            ->paginate((int) $request->input('limit', 10))
            ->appends($request->all());

        return response($medias, 200);
    }

    /**
     * Paginate files.
     *
     * @return void
     */
    public function files (Request $request) {
        $medias = Media::where('disk', '=', 'files')
            ->paginate((int) $request->input('limit', 10))
            ->appends($request->all());

        return response($medias, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return [\App\Models\Media]
     */
    public function show($id) {
        return(Media::find($id));
    }

    /**
     *  Search Medias
     * @param Request $request
     * @return type
     */
    public function search(Request $request)
    {   
        $input = $request->all();
        
        try {
        $medias = Media::search($input['q'])
            ->paginate((int) $request->input('limit', 10))
                ->appends($request->all());
        } catch (\Exception $e) {
            $e->getMessage();
            return response($e->getMessage(), 400);
        }

        return response($medias, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\Media
     */
    public function store(Request $request)
    {
        $file = $request->file('file');
        $data = [];

        try {
            // Populate common data
            $data['name'] = $file->getClientOriginalName();
            $data['file_name'] = str_random() . '.' . $file->getClientOriginalExtension();
            $data['size'] = $file->getSize();
            $data['data'] = ['mimeType' => $file->getMimeType()];

            // Vefiry if file is image type
            if (in_array($file->getMimeType(), ["image/gif", "image/jpeg", "image/png"])) {
                list($width, $height) = getimagesize($file);

                $data['disk'] = 'images';
                $data['data']['width'] = $width;
                $data['data']['height'] = $height;
            } else {
                $data['disk'] = 'files';
            }

            // Create media object
            $media = Media::create($data);

            // Save file to storage
            Storage::putFileAs(
                $data['disk'],
                $file,
                $data['file_name'],
                'public'
            );
        } catch(Exception $e) {
            return response($e->getMessage(), 400);
        }

        return response($media, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \App\Models\OAuth\Media
     */
    public function update(Request $request, $id){
        $input = $request->input('media');
        $media = Media::find($id);

        $media->update($input);

        return response($media, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id)
    {
        return response(Media::destroy($id), 200);
    }
}
