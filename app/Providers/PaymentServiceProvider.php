<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\PaymentGateways\PagseguroGateway;
use App\Services\PaymentGateways\CieloGateway;

class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\PaymentService', function ($app) {
            if(config('payment.default') == 'pagseguro'){
                $gateway = new PagseguroGateway();
            } elseif(config('payment.default') == 'cielo'){
                $gateway = new CieloGateway();
            }

            return new \App\Services\PaymentService($gateway);
        });
    }
}
