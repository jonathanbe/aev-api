<?php

use App\Models\User;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * User show route test.
     *
     * @return void
     */
    public function testShow()
    {
        $user = factory('App\Models\User')->create();

        $this->get('/users/' . $user->id, $this->getHeaders())
            ->seeJsonStructure([
                'username',
                'person' => [
                    'first_name', 'last_name', 'full_name', 'email', 'phone', 'mobile',
                    'address' => [
                        'street', 'complement', 'neighbourhood', 'zipcode', 'state', 'city', 'number', 'country'
                    ]
                ]
            ]);
    }

    /**
     * User create route test.
     *
     * @return void
     */
    public function testCreate()
    {
        $user = factory('App\Models\User')->raw(['person_id' => null]);
        $person = factory('App\Models\Person')->raw(['address_id' => null]);
        $address = factory('App\Models\Address')->raw();

        // Create user
        $this->post('/users', [
            'user' => $user,
            'address' => $address,
            'person' => $person,
            'password' => $user['password']
        ], $this->getHeaders())
        ->seeJson([
            'username' => $user['username']
        ]);
    }

    /**
     * User update route test.
     *
     * @return void
     */
    public function testUpdate()
    {
        $user = factory('App\Models\User')->create();

        // Update the user
        $this->put('/users/' . $user->id, [
            'user' => [
                'username' => 'giseudo',
            ],
            'person' => [
                'first_name' => 'Giseudo',
                'last_name' => 'Oliveira',
            ],
            'address' => [
                'street' => 'Rua São Miguel',
                'number' => 220,
                'neighbourhood' => 'Varadouro'
            ],
            'password' => '123123'
        ], $this->getHeaders())
        ->seeJson([
            'username' => 'giseudo'
        ]);

        // Check if person has changed
        $this->seeInDatabase('people', [
            'first_name' => 'Giseudo',
            'last_name' => 'Oliveira'
        ]);

        // Check if address has changed
        $this->seeInDatabase('addresses', [
            'street' => 'Rua São Miguel',
            'number' => 220,
            'neighbourhood' => 'Varadouro'
        ]);
    }

    /**
     * Paginate user route test.
     *
     * @return void
     */
    public function testPaginate()
    {
        factory('App\Models\User', 40)->create();

        $this->get('/users?limit=5', $this->getHeaders())
            ->seeJson([
                'per_page' => 5,
                'last_page' => 8,
                'total' => 40
            ])
            ->seeJsonStructure([
                'data' => [
                    ['person']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Search user route test.
     *
     * @return void
     */
    public function testSearch()
    {
        factory('App\Models\User', 40)->create();
        factory('App\Models\User')->create([
            'person_id' => factory('App\Models\Person')->create([
                'first_name' => 'Giseudo',
                'last_name' => 'Oliveira'
            ])->id
        ]);

        $this->get('/users/search?q=giseudo&limit=1', $this->getHeaders())
            ->seeJson([
                'per_page' => 1,
                'last_page' => 1,
                'total' => 1
            ])
            ->seeJsonStructure([
                'data' => [
                    ['person']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Delete user route test.
     *
     * @return void
     */
    public function testDelete()
    {
        $user = factory('App\Models\User')->create();

        // Delete the user
        $response = $this->delete('/users/' . $user->id, [], $this->getHeaders());

        // Check if user was not soft deleted
        $this->notSeeInDatabase('users', [
            'id' => $user->id,
            'deleted_at' => null
        ]);
    }
}
