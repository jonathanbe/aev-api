
<?php

use App\Models\Institution;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class InstitutionTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Institution show route test.
     *
     * @return void
     */
    public function testShow()
    {
        $institution = factory('App\Models\Institution')->create();

        $this->get('/institutions/' . $institution->id, $this->getHeaders())
            ->seeJsonStructure([
                'name', 'address_id', 'thumbnail_id', 'location', 'content',
                'address' => [
                    'street', 'neighbourhood', 'state', 'city', 'number',
                    'zipcode', 'country', 'country_id'
                ]
            ]);
    }

    /**
     * Institution create route test.
     *
     * @return void
     */
    public function testCreate()
    {
        $institution = factory('App\Models\Institution')->raw();
        $address = factory('App\Models\Address')->raw();

        $this->post('/institutions', [
            'institution' => $institution,
            'address' => $address
        ])
        ->seeJson([
            'name' => $institution['name']
        ])
        ->seeJsonStructure([
            'name', 'address', 'thumbnail', 'location', 'content'
        ]);
    }

    /**
     * Institution update route test.
     *
     * @return void
     */
    public function testUpdate()
    {
        $institution = factory('App\Models\Institution')->create();

        $this->put('/institutions/' . $institution->id, [
            'institution' => [
                'name' => 'Supernova'
            ],
            'address' => [
                'street' => 'Elm Street'
            ]
        ], $this->getHeaders())
        ->seeJson([
            'name' => 'Supernova'
        ])
        ->seeJsonStructure([
            'name', 'address', 'thumbnail', 'location', 'content'
        ]);

        $this->seeInDatabase('addresses', [
            'street' => 'Elm Street'
        ]);
    }

    /**
     * Paginate institution route test.
     *
     * @return void
     */
    public function testPaginate()
    {
        factory('App\Models\Institution', 40)->create();

        $this->get('/institutions?limit=5', $this->getHeaders())
            ->seeJson([
                'total' => 40,
                'per_page' => 5,
                'last_page' => 8
            ])
            ->seeJsonStructure([
                'data' => [
                    ['address', 'thumbnail', 'location', 'content']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Delete institution route test.
     *
     * @return void
     */
    public function testDelete()
    {
        $institution = factory('App\Models\Institution')->create();

        $this->delete('/institutions/' . $institution->id, [], $this->getHeaders());

        $this->notSeeInDatabase('institutions', [
            'id' => $institution->id,
            'deleted_at' => null
        ]);
    }
}
