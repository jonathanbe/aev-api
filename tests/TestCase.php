<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Notification;
use App\Models\OAuth\Client;

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = '';

    /**
     * The access token to the application.
     *
     * @var string
     */
    protected $token = '';

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    public function setUp()
    {
        parent::setUp();
        $this->baseUrl = env('API_URL', 'https://aev.dev/api');

        // Seed the database
        $this->artisan('migrate');
        $this->artisan('db:seed');

        // Prevents sending notifications
        Notification::fake();
   }

    public function getHeaders()
    {
        return [
            //
        ];
    }
}
