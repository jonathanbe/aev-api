
<?php

use App\Models\Media;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class MediaTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Media show route test.
     *
     * @return void
     */
    public function testShow()
    {
        $media = factory('App\Models\Media')->create();

        $this->get('/medias/' . $media->id, $this->getHeaders())
            ->seeJsonStructure([
                'name', 'caption', 'file_name', 'size', 'data',
                'disk', 'src'
            ]);
    }

    /**
     * Media create route test.
     *
     * @return void
     */
    public function testCreate()
    {
        Storage::fake('images');
        Storage::fake('files');

        $response = $this->call('POST', '/medias', [], [], [
            'file' => UploadedFile::fake()->image('avatar.jpg'),
        ], $this->getHeaders());

        // Check if request succeeded
        $this->assertEquals($response->status(), 200);

        // Get json response content
        $json = json_decode($response->content(), true);

        // Check if file exists on storage directory
        Storage::disk('images')->assertExists($json['file_name']);

        // Check if media was created
        $this->seeInDatabase('medias', [
            'file_name' => $json['file_name'],
            'data' => json_encode($json['data'])
        ]);
    }

    /**
     * Media update route test.
     *
     * @return void
     */
    public function testUpdate()
    {
        $media = factory('App\Models\Media')->create();

        $this->put('/medias/' . $media->id, [
            'media' => [
                'name' => 'A fancy name to a fancy rose.',
                'caption' => 'Does it really care?'
            ]
        ], $this->getHeaders())
        ->seeJson([
            'name' => 'A fancy name to a fancy rose.',
            'caption' => 'Does it really care?'
        ])
        ->seeJsonStructure([
            'name', 'caption', 'file_name', 'size', 'data',
            'disk', 'src'
        ]);
    }

    /**
     * Paginate media images route test.
     *
     * @return void
     */
    public function testPaginateImages()
    {
        factory('App\Models\Media','image', 40)->create();

        $this->get('/medias/images?limit=5', $this->getHeaders())
            ->seeJson([
                'total' => 40,
                'per_page' => 5,
                'last_page' => 8
            ])
            ->seeJsonStructure([
                'data' => [
                    ['src']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Paginate media files route test.
     *
     * @return void
     */
    public function testPaginateFiles()
    {
        factory('App\Models\Media', 'file', 40)->create();

        $this->get('/medias/files?limit=5', $this->getHeaders())
            ->seeJson([
                'total' => 40,
                'per_page' => 5,
                'last_page' => 8
            ])
            ->seeJsonStructure([
                'data' => [
                    ['src']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Search media route test.
     *
     * @return void
     */
    public function testSearch()
    {
        factory('App\Models\Media', 40)->create();
        factory('App\Models\Media')->create([
            'name' => 'Icicle disaster!'
        ]);
        
        $this->get('/medias/search?q=disaster&limit=1', $this->getHeaders())
            ->seeJson([
                'per_page' => 1,
                'last_page' => 1,
                'total' => 1
            ])
            ->seeJsonStructure([
                'data' => [
                    ['src']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Delete media route test.
     *
     * @return void
     */
    public function testDelete()
    {
        $media = factory('App\Models\Media')->create();

        $this->delete('/medias/' . $media->id, [], $this->getHeaders());

        $this->notSeeInDatabase('medias', [
            'id' => $media->id,
            'deleted_at' => null
        ]);
    }
}
