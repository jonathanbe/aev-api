<?php

use App\Models\Donation;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class DonationTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Donation show route test.
     *
     * @return void
     */
    public function testShow()
    {
        $donation = factory('App\Models\Donation')->create();

        $this->get('/donations/' . $donation->id, $this->getHeaders())
            ->seeJsonStructure([
                'institution', 'institution_id', 'invoice', 'invoice_id',
                'value', 'created_at'
            ]);
    }

    /**
     * Donation create route test.
     *
     * @return void
     */
    public function testCreate()
    {
        $donation = factory('App\Models\Donation')->raw(['invoice_id' => NULL]);
        $person = factory('App\Models\Person')->raw(['address_id' => NULL]);
        $address = factory('App\Models\Address')->raw();

        $this->post('/donations', [
            'donation' => $donation,
            'person' => $person,
            'address' => $address
        ], $this->getHeaders())
        ->seeJsonStructure([
            'redirect_uri', 'donation'
        ]);
    }

    /**
     * Paginate donation route test.
     *
     * @return void
     */
    public function testPaginate()
    {
        factory('App\Models\Donation', 40)->create();

        $this->get('/donations?limit=5', $this->getHeaders())
            ->seeJson([
                'total' => 40,
                'per_page' => 5,
                'last_page' => 8
            ])
            ->seeJsonStructure([
                'data' => [
                    ['institution', 'institution_id', 'invoice', 'invoice_id',
                    'value', 'created_at']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Search donation route test.
     *
     * @return void
     */
    public function testSearch()
    {
        factory('App\Models\Donation', 40)->create();
        factory('App\Models\Donation')->create([
            'invoice_id' => factory('App\Models\Invoice')->create([
                'person_id' => factory('App\Models\Person')->create([
                    'email' => 'giseudo@gmail.com'
                ])->id
            ])->id,
            'created_at' => '2020-09-20'
        ]);

        $this->get('/donations/search?q[init]=2020-09-20&limit=1', $this->getHeaders())
            ->seeJson([
                'per_page' => 1,
                'last_page' => 1,
                'total' => 1
            ])
            ->seeJsonStructure([
                'data' => [
                    ['institution', 'institution_id', 'invoice', 'invoice_id',
                    'value', 'created_at']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Delete donation route test.
     *
     * @return void
     */
    public function testDelete()
    {
        $donation = factory('App\Models\Donation')->create();

        $this->delete('/donations/' . $donation->id, [], $this->getHeaders());

        $this->notSeeInDatabase('donations', [
            'id' => $donation->id,
            'deleted_at' => null
        ]);
    }
}
