<?php

use App\Models\Partner;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class PartnerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Partner show route test.
     *
     * @return void
     */
    public function testShow()
    {
        $partner = factory('App\Models\Partner')->create();

        $this->get('/partners/' . $partner->id, $this->getHeaders())
            ->seeJsonStructure([
                'id', 'name', 'brand', 'brand_id'
            ]);
    }

    /**
     * Partner create route test.
     *
     * @return void
     */
    public function testCreate()
    {
        $partner = factory('App\Models\Partner')->raw();

        $this->post('/partners', [
            'partner' => $partner
        ], $this->getHeaders());

        $this->seeInDatabase('partners', [
            'name' => $partner['name']
        ]);
    }

    /**
     * Partner update route test.
     *
     * @return void
     */
    public function testUpdate()
    {
        $partner = factory('App\Models\Partner')->create();

        $this->put('/partners/' . $partner->id, [
            'partner' => [
                'name' => 'Petrobras'
            ]
        ], $this->getHeaders());

        $this->seeInDatabase('partners', [
            'name' => 'Petrobras'
        ]);
    }

    /**
     * Partner paginate route test.
     *
     * @return void
     */
    public function testPaginate()
    {
        factory('App\Models\Partner', 40)->create();

        $this->get('/partners?limit=5', $this->getHeaders())
            ->seeJson([
                'per_page' => 5,
                'last_page' => 8,
                'total' => 40
            ])
            ->seeJsonStructure([
                'data' => [
                    ['brand']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Search partner route test.
     *
     * @return void
     */
    public function testSearch()
    {
        factory('App\Models\Partner', 40)->create();
        factory('App\Models\Partner')->create(['name' => 'Petrobras']);

        $this->get('/partners/search?q=petrobras', $this->getHeaders())
            ->seeJson([
                'total' => 1
            ])
            ->seeJsonStructure([
                'data' => [
                    ['brand']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Delete user route test.
     *
     * @return void
     */
    public function testDelete()
    {
        $partner = factory('App\Models\Partner')->create();

        $this->delete('/partners/' . $partner->id, [], $this->getHeaders());

        // Check if partner was not soft deleted
        $this->notSeeInDatabase('partners', [
            'id' => $partner->id,
            'deleted_at' => null
        ]);
    }
}
