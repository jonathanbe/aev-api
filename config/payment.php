<?php
return [
    'default' => env('PAYMENT_DRIVER', 'pagseguro'),
    'gateways' => [
        'pagseguro' => [
            'driver' => 'transparent',
            'environment' => env('PAGSEGURO_ENV', 'sandbox'),
            'email' => env('PAGSEGURO_EMAIL'),
            'token' => env('PAGSEGURO_TOKEN'),
            'app_id' => env('PAGSEGURO_APP_ID'),
            'app_secret' => env('PAGSEGURO_APP_SECRET')
        ],
        'cielo' => [
            'driver' => 'transparent',
            'environment' => env('CIELO_ENV', 'sandbox'),
            'merchant_id' => env('CIELO_MERCHANT_ID'),
            'merchant_secret' => env('CIELO_MERCHANT_SECRET'),
            'id' => env('CIELO_ID'),
            'secret' => env('CIELO_KEY'),
        ],
    ]
];
