<?php

$factory->define(App\Models\Donation::class, function (Faker\Generator $faker) {
    return [
        'value' => 50000,
        'institution_id' => factory('App\Models\Institution')->create()->id,
        'invoice_id' => factory('App\Models\Invoice')->create()->id
    ];
});
