<?php

// Media
$factory->define(App\Models\Media::class, function (Faker\Generator $faker) {
    return $faker->boolean(50) ?
        factory('App\Models\Media', 'image')->raw() :
        factory('App\Models\Media', 'file')->raw();
});

// Image
$factory->defineAs(App\Models\Media::class, 'image' , function (Faker\Generator $faker) {
    return [
        'name' => $faker->catchPhrase,
        'caption' => $faker->sentence,
        'file_name' => $faker->slug . '.jpg',
        'disk' => 'images',
        'size' => $faker->numberBetween(2000, 10000),
        'data' => json_encode([
            'width' => 500,
            'height' => 300
        ])
    ];
});

// File
$factory->defineAs(App\Models\Media::class, 'file', function (Faker\Generator $faker) {
    return [
        'name' => $faker->catchPhrase,
        'caption' => $faker->sentence,
        'file_name' => $faker->slug . '.' . $faker->fileExtension,
        'disk' => 'files',
        'size' => $faker->numberBetween(2000, 10000),
        'data' => json_encode([])
    ];
});
