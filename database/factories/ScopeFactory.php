<?php

$factory->define(App\Models\OAuth\Scope::class, function (Faker\Generator $faker) {
    return [
        'id' => 'manage_' + $faker->word,
        'description' => $faker->sentence(6)
    ];
});
