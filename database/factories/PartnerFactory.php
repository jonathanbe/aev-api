<?php

$factory->define(App\Models\Partner::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company
    ];
});

