<?php

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->userName,
        'password' => $faker->password,
        'person_id' => factory('App\Models\Person')->create()->id
    ];
});

