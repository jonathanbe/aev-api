<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call('CountriesSeeder');

		if(env('APP_ENV') != 'testing'){
			$this->call('UserSeeder');
			$this->call('InstitutionSeeder');
		}
	}
}
