<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_media', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order');
            $table->integer('event_id')->nullable()->unsigned();
            $table->integer('media_id')->nullable()->unsigned();
            $table->string('caption');

            $table->foreign('event_id')
                ->unique()
                ->references('id')->on('events')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            
            $table->foreign('media_id')
                ->unique()
                ->references('id')->on('medias')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_media');
    }
}
